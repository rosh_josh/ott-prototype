class Login extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8 login__text__wrap">
                    <img src="https://i.pinimg.com/originals/e3/7a/9a/e37a9afd4423147c914c185de7647c44.png" alt="Movie Logo"/>
                </div>
                <div class="col-12 col-md-4 login__container">
                    <h1><span>Admin </span>Login</h1>
                    <label>Username</label>
                    <input type="text" required class="default__input">
                    <label>Password</label>
                    <input type="password" required class="default__input">
                    <a href="homepage.html"><button class="cta">Login</button></a>
                </div>
            </div>
        </div>
    `;
  }
}
class Navbar extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">OTT Dash</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
        
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="homepage.html">Videos<span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="subscribers.html">Subscribers</a>
                        </li>
                    </ul>
                    <button class="cta" id="upload"><i class="fas fa-upload"></i> Upload</button>
                    <div class="search__wrapper">
                        <input type="text" required placeholder="Search" />
                        <i class="fas fa-search"></i>
                    </div>
                    <a class="nav-link" href="#"><i class="far fa-bell"></i></a>
                    <a class="nav-link" href="#"><img src="./assets/img/profile.jpg" class="profile__pic" /></a>
                </div>
            </div>
        </nav>
    `;
  }
}
class Movie extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
          <div class="col-12 img__card">
              <img src="./assets/img/cover-6.jpg" alt="Movie List" />
              <span>Title </span>
              <ul class="video--meta">
                  <li>meta tag</li>
                  <li>meta tag</li>
                  <li>meta tag</li>
              </ul>
          </div>
          <div class="img__card__overlay">
              <i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit
          </div>
    `;
  }
}
class UploadVideo extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
          <div class="modal__show">
            <div class="upload__container">
                <div class="upload--div">
                    <i class="fas fa-times" id="close__modal"></i>
                    <div class="col-12 modal__content">
                        <div class="row">
                            <div class="col-12 col-md-4 upload--box">
                                <div class="col-12 upload--vid--sec">
                                    <i class="fas fa-file-upload"></i>
                                    <span><b>Upload</b> Videos</span>
                                </div>
                                
                            </div>
                            <div class="col-12 col-md-8 upload--meta">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <label>Title</label>
                                        <input type="text"/>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label>Category <i class="fas fa-plus-circle" aria-hidden="true"></i></label>
                                        <select>
                                            <option>Action</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label>Description for Video</label>
                                        <textarea></textarea>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label>Select Tags <i class="fas fa-plus-circle" aria-hidden="true"></i></label>
                                        <select multiple class="multiSel">
                                            <option>Tag 1</option>
                                            <option>Tag 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="cta--alt">Cancel</button>
                                <button class="cta float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
  }
}

customElements.define('login-page', Login);
customElements.define('navbar-app', Navbar);
customElements.define('movie-list', Movie);
customElements.define('upload-video', UploadVideo);

